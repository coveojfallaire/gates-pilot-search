'use strict';
/**
 * Module dependencies.
 */
const passport = require('passport');
const OIDCStrategy = require('passport-azure-ad').OIDCStrategy;
const config = require('./config');

const azureCfg = config.auth.azure[process.env.AUTH_APP || 'devLocal'];

// console.log('using AUTH_APP [' + process.env.AUTH_APP + '] with the following redirectURL: ' +  azureCfg.redirectUrl)


// Passport session setup. (Section 2)

//   To support persistent login sessions, Passport needs to be able to
//   serialize users into and deserialize users out of the session.  Typically,
//   this will be as simple as storing the user ID when serializing, and finding
//   the user by ID when deserializing.
passport.serializeUser(function (user, done) {
    // console.log('entering serializeUser >>>' + JSON.stringify(user));
    done(null, user.oid);
});

passport.deserializeUser(function (oid, done) {
    // console.log('entering deserializeUser >>>' + oid);
    findByOid(oid, function (err, user) {
        done(err, user);
    });
});

// array to hold logged in users
var users = [];

var findByOid = function (oid, fn) {
    // console.log('entering findByOid >>>' + oid);
    for (var i = 0, len = users.length; i < len; i++) {
        var user = users[i];
        // console.log('we are using user: ', user);
        if (user.oid === oid) {
            // console.log('found a match, returning user >>>' + user);
            return fn(null, user);
        }
    }
    return fn(null, null);
};

// Use the OIDCStrategy within Passport. (Section 2) 
// 
//   Strategies in passport require a `validate` function, which accept
//   credentials (in this case, an OpenID identifier), and invoke a callback
//   with a user object.
passport.use(new OIDCStrategy({
    identityMetadata: azureCfg.identityMetadata,
    clientID: azureCfg.clientID,
    responseType: azureCfg.responseType,
    responseMode: azureCfg.responseMode,
    redirectUrl: azureCfg.redirectUrl,
    allowHttpForRedirectUrl: azureCfg.allowHttpForRedirectUrl,
    clientSecret: azureCfg.clientSecret,
    validateIssuer: azureCfg.validateIssuer,
    isB2C: azureCfg.isB2C,
    issuer: azureCfg.issuer,
    passReqToCallback: azureCfg.passReqToCallback,
    scope: azureCfg.scope,
    loggingLevel: azureCfg.loggingLevel,
    nonceLifetime: azureCfg.nonceLifetime,
    nonceMaxAmount: azureCfg.nonceMaxAmount,
    useCookieInsteadOfSession: azureCfg.useCookieInsteadOfSession,
    cookieEncryptionKeys: azureCfg.cookieEncryptionKeys,
    clockSkew: azureCfg.clockSkew,
},
    function (iss, sub, profile, accessToken, refreshToken, done) {
        if (!profile.oid) {
            return done(new Error("No oid found"), null);
        }
        // asynchronous verification, for effect...
        process.nextTick(function () {
            // console.log('profile.oid >>>' + profile.oid)
            findByOid(profile.oid, function (err, user) {
                // console.log('err, user >>>' + err + '||' + user);
                if (err) {
                    return done(err);
                }
                if (!user) {
                    // "Auto-registration"
                    // console.log('pushing profile to users >>>' + profile);
                    users.push(profile);
                    return done(null, profile);
                }
                // console.log('Ending Method for profiling');
                return done(null, user);
            });
        });
    }
));

// Simple route middleware to ensure user is authenticated. (Section 4)

//   Use this route middleware on any resource that needs to be protected.  If
//   the request is authenticated (typically via a persistent login session),
//   the request will proceed.  Otherwise, the user will be redirected to the
//   login page.
passport.ensureAuthenticated = function ensureAuthenticated(req, res, next) {
    // console.log('req.isAuthenticated() >>>' + req.isAuthenticated());
    // console.log(req.user);
    // console.log(req.session);
    if (req.isAuthenticated()) { return next(); }
    res.redirect('/login');
}

exports = module.exports = passport;
