'use strict';
const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const compression = require('compression');
const methodOverride = require('method-override');
const cookieParser = require('cookie-parser');
const server = express();
const cfg = require('./config');
const path = require('path');
const auth = require('./auth_azure');

// set the view engine to ejs
server.set('view engine', 'ejs');

server.use(compression());
server.use(methodOverride());
server.use(cookieParser());
server.use(session({ secret: 'keyboard cat', resave: true, saveUninitialized: false, cookie: { maxAge: 3600*1000 } }));
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));
server.use(auth.initialize());
server.use(auth.session());
server.use(express.static(path.resolve('./public')));
server.use(require('./routes/pilot-azure-ad'));

server.listen(cfg.server_port);


