var config = require('./config.global');

config.env = 'production';

//Coveo
config.coveo.api_key = 'xxfa212a85-8bc7-4218-beaa-2704f4949643';
config.coveo.org_id = 'billmelindagatesfoundation';
config.coveo.filter = process.env.FILTER_EXPRESSION || '(NOT @filetype==DS_Store) (@owsid OR (@commonsource=="Salesforce" (NOT (@sfcontactownerrolec=PPT OR @sfrecordtypename=PPT))))';

module.exports = config;