var config = module.exports = {};
const minimize = process.argv.indexOf('--minimize') !== -1;
const path = require('path');

config.env = 'development';
config.enableImpersonateUser = false;
config.hostname = 'dev.example.com';
config.server_port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 3000;
config.server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';

// Authentication
config.auth = {};

// If you want to use the mongoDB session store for session middleware; otherwise we will use the default
// session store provided by express-session.
// Note that the default session store is designed for development purpose only.
config.auth.useMongoDBSessionStore = false;
// If you want to use mongoDB, provide the uri here for the database.
config.auth.databaseUri = 'mongodb://localhost/OIDCStrategy';
// How long you want to keep session in mongoDB.
config.auth.mongoDBSessionMaxAge = 24 * 60 * 60;  // 1 day (unit is second)

// OKTA Authentication configurations
config.auth.okta = {};
//config.auth.okta.api_key = '00TPjRevY2gXoiLltrf4g-H21KNHvPegtC4mqScWaC';
config.auth.okta.devLocal = {
    issuer : 'http://localhost:3000',
    path: '/login/callback',
    entryPoint : 'https://dev-253013.oktapreview.com/app/coveodev253013_gatespilotdevjfa_1/exkaaflywjpDuT57h0h7/sso/saml',
    logoutUrl : 'https://dev-253013.oktapreview.com/app/coveodev253013_gatespilotdevjfa_1/exkaaflywjpDuT57h0h7/slo/saml',
    cert : 'MIIDpDCCAoygAwIBAgIGAVnTu33/MA0GCSqGSIb3DQEBBQUAMIGSMQswCQYDVQQGEwJVUzETMBEGA1UECAwKQ2FsaWZvcm5pYTEWMBQGA1UEBwwNU2FuIEZyYW5jaXNjbzENMAsGA1UECgwET2t0YTEUMBIGA1UECwwLU1NPUHJvdmlkZXIxEzARBgNVBAMMCmRldi0yNTMwMTMxHDAaBgkqhkiG9w0BCQEWDWluZm9Ab2t0YS5jb20wHhcNMTcwMTI1MDM0NTM3WhcNMjcwMTI1MDM0NjM3WjCBkjELMAkGA1UEBhMCVVMxEzARBgNVBAgMCkNhbGlmb3JuaWExFjAUBgNVBAcMDVNhbiBGcmFuY2lzY28xDTALBgNVBAoMBE9rdGExFDASBgNVBAsMC1NTT1Byb3ZpZGVyMRMwEQYDVQQDDApkZXYtMjUzMDEzMRwwGgYJKoZIhvcNAQkBFg1pbmZvQG9rdGEuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhNBQfDp4fHANo/DVUJcf0wv9l7P+1WUKvaIArWZjPckmMJiqkMSXIR4yeq2nq13cDdepQByamtjHra3gJXPw/+5TsDFs1AjLkor5Q6rJulre5a6ALqLxDVseI1IkQvCkITT1VP02Qpt7bTVC9sM0tQCS7wVgniElTCbfdi66sKtqFC8wWA5QlaxmLdVpo9ccZE7WD/ROdEbgoYFwqoO4DaPLPYsx8LcW7SoTQDK39L0NsWHtRgW1WY/2i9C9SKzp+63Bt3+cIzbAC0E7iSqzmY/jdNe5b2iWuY2UqGjmQZtga8nB6Dgy6WTcFm6MuKgUGcM+ptKshokgUaC9Ops15QIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQA2Pa7HAIuwIKLdEGJ0thIZsqBEsiQGBFVYcCgCemPsz8PYg1oxXxHOO7e9ahpxB8t3+2aS0y1oXqqHtr2QhYEA03i8pKDntNZhU76kZSCZDLrzrHRzgp0OlkKQFjSQuLAmjORYqsMdxIc51qKUc84GzRZOXsprM0AMwGYIrJI1QVqXm1Q8WFF79e7N/VvVKutkHYAC9QimizutSSaH+mouBbGeVMrXdCjSs+IOv57eUiuHB19OxxVLVxKky4now73L9tGQ/229WVpawAHh9tgGbY+R79hxHo8YlSEoG94Ida9bIGy6LPkRDt8McZi5nMRrKN2FaCCLR8TV5A2tcN0Q'
};

config.auth.okta.devAzure = {
    issuer : 'http://gates-search-ui-dev-jfa.azurewebsites.net/',
    path: '/login/callback',
    entryPoint : 'https://dev-253013.oktapreview.com/app/coveodev253013_gatespilotdevjfaazure_1/exkaasymnhRqG59km0h7/sso/saml',
    cert : 'MIIDpDCCAoygAwIBAgIGAVnTu33/MA0GCSqGSIb3DQEBBQUAMIGSMQswCQYDVQQGEwJVUzETMBEGA1UECAwKQ2FsaWZvcm5pYTEWMBQGA1UEBwwNU2FuIEZyYW5jaXNjbzENMAsGA1UECgwET2t0YTEUMBIGA1UECwwLU1NPUHJvdmlkZXIxEzARBgNVBAMMCmRldi0yNTMwMTMxHDAaBgkqhkiG9w0BCQEWDWluZm9Ab2t0YS5jb20wHhcNMTcwMTI1MDM0NTM3WhcNMjcwMTI1MDM0NjM3WjCBkjELMAkGA1UEBhMCVVMxEzARBgNVBAgMCkNhbGlmb3JuaWExFjAUBgNVBAcMDVNhbiBGcmFuY2lzY28xDTALBgNVBAoMBE9rdGExFDASBgNVBAsMC1NTT1Byb3ZpZGVyMRMwEQYDVQQDDApkZXYtMjUzMDEzMRwwGgYJKoZIhvcNAQkBFg1pbmZvQG9rdGEuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhNBQfDp4fHANo/DVUJcf0wv9l7P+1WUKvaIArWZjPckmMJiqkMSXIR4yeq2nq13cDdepQByamtjHra3gJXPw/+5TsDFs1AjLkor5Q6rJulre5a6ALqLxDVseI1IkQvCkITT1VP02Qpt7bTVC9sM0tQCS7wVgniElTCbfdi66sKtqFC8wWA5QlaxmLdVpo9ccZE7WD/ROdEbgoYFwqoO4DaPLPYsx8LcW7SoTQDK39L0NsWHtRgW1WY/2i9C9SKzp+63Bt3+cIzbAC0E7iSqzmY/jdNe5b2iWuY2UqGjmQZtga8nB6Dgy6WTcFm6MuKgUGcM+ptKshokgUaC9Ops15QIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQA2Pa7HAIuwIKLdEGJ0thIZsqBEsiQGBFVYcCgCemPsz8PYg1oxXxHOO7e9ahpxB8t3+2aS0y1oXqqHtr2QhYEA03i8pKDntNZhU76kZSCZDLrzrHRzgp0OlkKQFjSQuLAmjORYqsMdxIc51qKUc84GzRZOXsprM0AMwGYIrJI1QVqXm1Q8WFF79e7N/VvVKutkHYAC9QimizutSSaH+mouBbGeVMrXdCjSs+IOv57eUiuHB19OxxVLVxKky4now73L9tGQ/229WVpawAHh9tgGbY+R79hxHo8YlSEoG94Ida9bIGy6LPkRDt8McZi5nMRrKN2FaCCLR8TV5A2tcN0Q'
};

config.auth.okta.bmgfdev = {
    issuer: 'http://bmgfdevsearch.azurewebsites.net',
    path: '/login/callback',
    entryPoint : 'https://dev-253013.oktapreview.com/app/coveodev253013_gatespilotbmgfdev_1/exkafraybtORd0Gt10h7/sso/saml',
    cert : 'MIIDpDCCAoygAwIBAgIGAVnTu33/MA0GCSqGSIb3DQEBBQUAMIGSMQswCQYDVQQGEwJVUzETMBEGA1UECAwKQ2FsaWZvcm5pYTEWMBQGA1UEBwwNU2FuIEZyYW5jaXNjbzENMAsGA1UECgwET2t0YTEUMBIGA1UECwwLU1NPUHJvdmlkZXIxEzARBgNVBAMMCmRldi0yNTMwMTMxHDAaBgkqhkiG9w0BCQEWDWluZm9Ab2t0YS5jb20wHhcNMTcwMTI1MDM0NTM3WhcNMjcwMTI1MDM0NjM3WjCBkjELMAkGA1UEBhMCVVMxEzARBgNVBAgMCkNhbGlmb3JuaWExFjAUBgNVBAcMDVNhbiBGcmFuY2lzY28xDTALBgNVBAoMBE9rdGExFDASBgNVBAsMC1NTT1Byb3ZpZGVyMRMwEQYDVQQDDApkZXYtMjUzMDEzMRwwGgYJKoZIhvcNAQkBFg1pbmZvQG9rdGEuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhNBQfDp4fHANo/DVUJcf0wv9l7P+1WUKvaIArWZjPckmMJiqkMSXIR4yeq2nq13cDdepQByamtjHra3gJXPw/+5TsDFs1AjLkor5Q6rJulre5a6ALqLxDVseI1IkQvCkITT1VP02Qpt7bTVC9sM0tQCS7wVgniElTCbfdi66sKtqFC8wWA5QlaxmLdVpo9ccZE7WD/ROdEbgoYFwqoO4DaPLPYsx8LcW7SoTQDK39L0NsWHtRgW1WY/2i9C9SKzp+63Bt3+cIzbAC0E7iSqzmY/jdNe5b2iWuY2UqGjmQZtga8nB6Dgy6WTcFm6MuKgUGcM+ptKshokgUaC9Ops15QIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQA2Pa7HAIuwIKLdEGJ0thIZsqBEsiQGBFVYcCgCemPsz8PYg1oxXxHOO7e9ahpxB8t3+2aS0y1oXqqHtr2QhYEA03i8pKDntNZhU76kZSCZDLrzrHRzgp0OlkKQFjSQuLAmjORYqsMdxIc51qKUc84GzRZOXsprM0AMwGYIrJI1QVqXm1Q8WFF79e7N/VvVKutkHYAC9QimizutSSaH+mouBbGeVMrXdCjSs+IOv57eUiuHB19OxxVLVxKky4now73L9tGQ/229WVpawAHh9tgGbY+R79hxHo8YlSEoG94Ida9bIGy6LPkRDt8McZi5nMRrKN2FaCCLR8TV5A2tcN0Q'
}

// Azure AD Authentication configurations
config.auth.azure = {
    devLocal : {
        redirectUrl: 'http://localhost:3000/login/callback',
        identityMetadata: 'https://login.microsoftonline.com/jfallairecoveo.onmicrosoft.com/.well-known/openid-configuration', // For using Microsoft you should never need to change this.
        clientID: 'f9905ade-22a0-4db7-b0d2-f58a172ba315',
        clientSecret: '1bXyJwxfbxMrYKPvyonEhXZ1QtIvtBDtbHRSEXNxJbk=', // if you are doing code or id_token code
        responseType: 'code id_token', // for login only flows use id_token. For accessing resources use `id_token code`
        responseMode: 'form_post', // For login only flows we should have token passed back to us in a POST
        // Required if we use http for redirectUrl
        allowHttpForRedirectUrl: true,
        // Required to set to false if you don't want to validate issuer
        validateIssuer: true,
        // Required if you want to provide the issuer(s) you want to validate instead of using the issuer from metadata
        issuer: null,
        // Required to set to true if the `verify` function has 'req' as the first parameter
        passReqToCallback: false,
        // Recommended to set to true. By default we save state in express session, if this option is set to true, then
        // we encrypt state and save it in cookie instead. This option together with { session: false } allows your app
        // to be completely express session free.
        useCookieInsteadOfSession: false,
        scope: null,
        // scope: ['email', 'profile'] // additional scopes you may wish to pass
        // Required if `useCookieInsteadOfSession` is set to true. You can provide multiple set of key/iv pairs for key
        // rollover purpose. We always use the first set of key/iv pair to encrypt cookie, but we will try every set of
        // key/iv pair to decrypt cookie. Key can be any string of length 32, and iv can be any string of length 12.
        cookieEncryptionKeys: [ 
          { 'key': '12345678901234567890123456789012', 'iv': '123456789012' },
          { 'key': 'abcdefghijklmnopqrstuvwxyzabcdef', 'iv': 'abcdefghijkl' }
        ],
        // Optional, 'error', 'warn' or 'info'
        loggingLevel: 'info',

        // Optional. The lifetime of nonce in session or cookie, the default value is 3600 (seconds).
        nonceLifetime: null,

        // Optional. The max amount of nonce saved in session or cookie, the default value is 10.
        nonceMaxAmount: 5,

        // Optional. The clock skew allowed in token validation, the default value is 300 seconds.
        clockSkew: null,
        destroySessionUrl: 'https://login.microsoftonline.com/common/oauth2/logout?post_logout_redirect_uri=http://localhost:3000/login'
    },
    devJFA : {
        redirectUrl: 'https://gates-search-ui-dev-jfa.azurewebsites.net/login/callback',
        identityMetadata: 'https://login.microsoftonline.com/jfallairecoveo.onmicrosoft.com/.well-known/openid-configuration',
        clientID: '301e4f6b-a499-49a4-a5f7-16d92aacd255',
        clientSecret: 'aPmyqVlQE7JWEfyks+YMlg/bWiGTnudkDuQkvF5Bppk=',
        responseType: 'code id_token', 
        responseMode: 'form_post',
        allowHttpForRedirectUrl: false,
        validateIssuer: true,
        issuer: null,
        passReqToCallback: false,
        useCookieInsteadOfSession: false,
        scope: null,
        loggingLevel: 'info',
        nonceLifetime: null,
        nonceMaxAmount: 5,
        clockSkew: null,
        destroySessionUrl: 'https://login.microsoftonline.com/common/oauth2/logout?post_logout_redirect_uri=https://gates-search-ui-dev-jfa.azurewebsites.net/login'
    },
    devAzure : {
        redirectUrl: 'https://gates-search-ui.azurewebsites.net/login/callback',
        identityMetadata: 'https://login.microsoftonline.com/jfallairecoveo.onmicrosoft.com/.well-known/openid-configuration',
        clientID: 'bebd71f4-c965-4edb-b62d-debaffcc08c5',
        clientSecret: 'opsrUkvzarNhkSy2L2/1mryoIGLv0Pk9dtboj7y9bng=',
        responseType: 'code id_token', 
        responseMode: 'form_post',
        allowHttpForRedirectUrl: false,
        validateIssuer: true,
        issuer: null,
        passReqToCallback: false,
        useCookieInsteadOfSession: false,
        scope: null,
        loggingLevel: 'info',
        nonceLifetime: null,
        nonceMaxAmount: 5,
        clockSkew: null,
        destroySessionUrl: 'https://login.microsoftonline.com/common/oauth2/logout?post_logout_redirect_uri=https://gates-search-ui.azurewebsites.net/login'
    },
    bmgfdev : {
        redirectUrl: 'https://bmgfdevsearch.azurewebsites.net/login/callback',
        identityMetadata: 'https://login.microsoftonline.com/76e06d28-3395-47bd-9888-1188cd04dd77/.well-known/openid-configuration',
        clientID: 'd5409f82-4476-4fa2-8f48-87231a30ddda',
        //clientSecret: 'YPrE3oupVMzo5lZbFPgncpebXsrCJz8EEoT43JTPPjA=',
        clientSecret: 'zRn6pbU1otbfsmvE/nKOVbAhmdRsodPqG89zNb0qZbI=',
        responseType: 'code id_token', 
        responseMode: 'form_post',
        allowHttpForRedirectUrl: false,
        validateIssuer: true,
        issuer: null,
        passReqToCallback: false,
        useCookieInsteadOfSession: false,
        scope: null,
        loggingLevel: 'info',
        nonceLifetime: null,
        nonceMaxAmount: 5,
        clockSkew: null,
        destroySessionUrl: 'https://login.microsoftonline.com/common/oauth2/logout?post_logout_redirect_uri=https://bmgfdevsearch.azurewebsites.net/login'
    },
    bmgfstg : {
        redirectUrl: 'https://bmgfstgsearch.azurewebsites.net/login/callback',
        identityMetadata: 'https://login.microsoftonline.com/296b3838-4bd5-496c-bd4b-f456ea743b74/.well-known/openid-configuration',
        clientID: '6742ece9-e64c-4751-bacb-b4c66d414378',
        //clientSecret: 'WicO48kovWFlW4u8aQb4AppU+f6FFaGqIjJlUUesgdA=',
        clientSecret: '0cmQfKHB1w7TrmBZOXlx9Zb9k3w2VD6FiEfYYrH72nw=',
        responseType: 'code id_token', 
        responseMode: 'form_post',
        allowHttpForRedirectUrl: false,
        validateIssuer: true,
        issuer: null,
        passReqToCallback: false,
        useCookieInsteadOfSession: false,
        scope: null,
        loggingLevel: 'info',
        nonceLifetime: null,
        nonceMaxAmount: 5,
        clockSkew: null,
        destroySessionUrl: 'https://login.microsoftonline.com/common/oauth2/logout?post_logout_redirect_uri=https://bmgfstgsearch.azurewebsites.net/login'
    },
    bmgfprod : {
        redirectUrl: 'https://find.gatesfoundation.org/login/callback',
        identityMetadata: 'https://login.microsoftonline.com/296b3838-4bd5-496c-bd4b-f456ea743b74/.well-known/openid-configuration',
        clientID: 'e34c0bc0-a42f-4fc0-b113-6bef30d19f9f',
        //clientSecret: 'WicO48kovWFlW4u8aQb4AppU+f6FFaGqIjJlUUesgdA=',
        clientSecret: 'd+noM85HUkyAMsytwUkDZNSdkRHlcigsQkVPPP0aTpM=',
        responseType: 'code id_token', 
        responseMode: 'form_post',
        allowHttpForRedirectUrl: false,
        validateIssuer: true,
        issuer: null,
        passReqToCallback: false,
        useCookieInsteadOfSession: false,
        scope: null,
        loggingLevel: 'info',
        nonceLifetime: null,
        nonceMaxAmount: 5,
        clockSkew: null,
        destroySessionUrl: 'https://login.microsoftonline.com/common/oauth2/logout?post_logout_redirect_uri=https://find.gatesfoundation.org/login'
    }
}

// coveo
config.coveo = {};
config.coveo.rest_uri = 'https://platform.cloud.coveo.com/rest/search';
config.coveo.cloud_platform_host = 'platform.cloud.coveo.com';
config.coveo.ops_identity = { 'name': 'jfallaire@coveo.com', 'provider': 'Email Security Provider' };
config.coveo.filter = '';

// custom
config.gates = {};
config.gates.webpack_config = {};

// webpack config for pilote bundle (Azure) -- PHASE 0
config.gates.webpack_config.azure = {
    entry: ['./src/Index.ts'],
    output: {
        path: path.resolve('./azureDist/public/js'),
        filename: minimize ? 'Coveo.Gates.min.js' : 'Coveo.Gates.js',
        libraryTarget: 'umd',
        library: 'CoveoExtension',
        publicPath: '/js/'
    }
};
// webpack config for support bundle -- PHASE 1
config.gates.webpack_config.support = {
    entry: ['./src/Index.ts'],
    output: {
        path: path.resolve('./bin/js'),
        filename: minimize ? 'Coveo.Gates.min.js' : 'Coveo.Gates.js',
        libraryTarget: 'umd',
        library: 'CoveoExtension',
        publicPath: '/js/'
    }
};


// Sample 
// webpack config for customer bundle -- PHASE 2
// config.gates.webpack_config.customer = {
//     entry: ['./src/Index.ts'],
//     output: {
//         path: path.resolve('./bin/js'),
//         filename: minimize ? 'Coveo.Gates.Customer.min.js' : 'Coveo.Gates.Customer.js',
//         libraryTarget: 'umd',
//         library: 'CoveoExtension',
//         publicPath: '/js/'
//     }
// };

// Sample
// webpack config for internal bundle -- PHASE 3
// config.gates.webpack_config.internal = {
//     entry: ['./src/Index.ts'],
//     output: {
//         path: path.resolve('./bin/js'),
//         filename: minimize ? 'Coveo.Gates.Internal.min.js' : 'Coveo.Gates.Internal.js',
//         libraryTarget: 'umd',
//         library: 'CoveoExtension',
//         publicPath: '/js/'
//     }
// };