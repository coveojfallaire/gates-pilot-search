var config = require('./config.global');

config.env = 'uat';
config.enableImpersonateUser = true;

/* Coveo */
// API Key Gates Sandbox 2
config.coveo.api_key = 'xxa6d0697e-dc93-4455-b663-b505c38fba39';
config.coveo.org_id = 'billmelindagatesfoundationsandbox2';
config.coveo.filter = process.env.FILTER_EXPRESSION || '(NOT @filetype==DS_Store) (@owsid OR (@commonsource=="Salesforce" (NOT (@sfcontactownerrolec=PPT OR @sfrecordtypename=PPT))))';

module.exports = config;