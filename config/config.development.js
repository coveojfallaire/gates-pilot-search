var config = require('./config.global');

config.env = 'development';
config.enableImpersonateUser = true;

/* Coveo */
// API Key Gates Sandbox 1
config.coveo.api_key = 'xx2e2925be-f269-407d-90fc-9401b2e01fbb';
config.coveo.expired_token = 'eyJhbGciOiJIUzI1NiJ9.eyJmaWx0ZXIiOiJAb3dzaWQgT1IgKEBzeXNzb3VyY2U9PVwiU2FsZXNmb3JjZSBGdWxsQ29weVwiIChOT1QgKEBzZmNvbnRhY3Rvd25lcnJvbGVjPVBQVCBPUiBAc2ZyZWNvcmR0eXBlbmFtZT1QUFQpKSkiLCJzZWFyY2hIdWIiOiJwaWxvdCIsInY4Ijp0cnVlLCJvcmdhbml6YXRpb24iOiJiaWxsbWVsaW5kYWdhdGVzZm91bmRhdGlvbnNhbmRib3gxIiwidXNlcklkcyI6W3sicHJvdmlkZXIiOiJFbWFpbCBTZWN1cml0eSBQcm92aWRlciIsIm5hbWUiOiJqZmFsbGFpcmVAY292ZW8uY29tIiwidHlwZSI6IlVzZXIifV0sInJvbGVzIjpbInF1ZXJ5RXhlY3V0b3IiXSwiZXhwIjoxNDk3NDc0MzA2LCJpYXQiOjE0OTczODc5MDZ9.DXAZ5ftzKiC_447RaIUXpbJ7D91oLB3VYYYasPPq_6Y';
config.coveo.org_id = 'billmelindagatesfoundationsandbox1';
config.coveo.filter = process.env.FILTER_EXPRESSION || '(NOT @filetype==DS_Store) (@owsid OR (@commonsource=="Salesforce" (NOT (@sfcontactownerrolec=PPT OR @sfrecordtypename=PPT))))';

module.exports = config;