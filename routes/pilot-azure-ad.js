'use strict';
var express = require('express');
var router = express.Router();
var auth = require('../auth_azure');
var cfg = require('../config');
var coveoPlatformApi = require('../utils/cloudplatformAPI');
var sessionUtils = require('../utils/sessionUtils');

var azureCfg = cfg.auth.azure[process.env.AUTH_APP || 'devLocal'];

router.get('/pilot-search', auth.ensureAuthenticated, function(req, res) {
    res.redirect('/');
});

router.get('/', auth.ensureAuthenticated, function(req, res) {
    console.log('entering pilot search!!!');
    console.log(req.user);

    let users = sessionUtils.getUserIdentities(req, 'azure');

    coveoPlatformApi.getSearchToken(users,cfg.coveo.filter)
        .then((data) => {
            const json = JSON.parse(data);
            console.log(json.token)
            res.render('pages/pilot-search', { 
                prototypeTitle : 'Pilot Search',
                config: cfg,
                token: json.token
            });
        })
        .catch((err) => {
            console.error(err);
            res.send('Error: unable to generate a valid Search token with the following users >>> ' + JSON.stringify(users));
        });
});

router.post('/renewtoken', auth.ensureAuthenticated, function(req, res) {

    let users = sessionUtils.getUserIdentities(req, 'azure');

    coveoPlatformApi.getSearchToken(users, cfg.coveo.filter)
        .then((data) => {
            const json = JSON.parse(data);
            console.log(json.token)
            res.send(json.token);
        })
        .catch((err) => {
            console.error(err);
            res.send('Error: unable to generate a valid Search token with the following users >>> ' + JSON.stringify(users));
        });
});

// router.get('/', function(req, res) {
//     console.log(req.user);
//     console.log(req.session);
//     res.send('Hello ' + req.session.passport.user + '!!!');
// });

// 'logout' route, logout from passport, and destroy the session with AAD.
router.get('/logout', function(req, res){
  req.session.destroy(function(err) {
    req.logOut();
    res.redirect(azureCfg.destroySessionUrl);
  });
});

router.get('/login', 
    function(req, res, next) {
        auth.authenticate('azuread-openidconnect', { response: res,  failureRedirect: '/' })(req, res, next);
      },
      function(req, res) {
        console.log('Login was called.');
        res.redirect('/pilot-search');
    });

router.post('/azure/login/callback', 
    function(req, res, next) {
        auth.authenticate('azuread-openidconnect', { response: res, failureRedirect: '/'  })(req, res, next);
    },
    function(req, res) {
        console.log('We received a return from AzureAD.');
        res.redirect('/pilot-search');
    }); 

router.get('/azure/login/callback',
    function (req, res, next) {
        auth.authenticate('azuread-openidconnect', { response: res, failureRedirect: '/' })(req, res, next);
    },
    function (req, res) {
        console.log('We received a return from AzureAD.');
        res.redirect('/');
    });

router.post('/login/callback', 
    function(req, res, next) {
        auth.authenticate('azuread-openidconnect', { response: res, failureRedirect: '/'  })(req, res, next);
    },
    function(req, res) {
        console.log('We received a return from AzureAD.');
        res.redirect('/');
    }); 

router.get('/login/callback',
    function (req, res, next) {
        auth.authenticate('azuread-openidconnect', { response: res, failureRedirect: '/' })(req, res, next);
    },
    function (req, res) {
        console.log('We received a return from AzureAD.');
        res.redirect('/');
    });


module.exports = router;