'use strict';
var express = require('express');
var router = express.Router();
var auth = require('../auth_okta');
var cfg = require('../config');
var coveoPlatformApi = require('../utils/cloudplatformAPI');
var sessionUtils = require('../utils/sessionUtils');

router.get('/pilot-search', auth.ensureAuthenticated, function(req, res) {
    console.log('entering pilot search!!!');
    console.log(req.session.passport.user);

    let users = sessionUtils.getUserIdentities(req, 'okta');

    coveoPlatformApi.getSearchToken(users)
        .then((data) => {
            const json = JSON.parse(data);
            console.log(json.token)
            res.render('pages/pilot-search', { 
                prototypeTitle : 'Pilot Search',
                config: cfg,
                token: json.token
            });
        })
        .catch((err) => console.error(err));
});

router.get('/logout', (req, res) => {
    req.logout();
    req.session.destroy(function (err) {
        res.redirect('/login'); //Inside a callback… bulletproof!
    });
})

//auth.authenticate check if you are logged in
router.get('/login', auth.authenticate('saml', { failureRedirect: '/', failureFlash: true }), function(req, res) {
    res.redirect('/pilot-search');
});

//POST Methods, redirect to home successful login
router.post('/login/callback', auth.authenticate('saml', { failureRedirect: '/', failureFlash: true }), function(req, res) {
    res.redirect('/pilot-search');
});


module.exports = router;