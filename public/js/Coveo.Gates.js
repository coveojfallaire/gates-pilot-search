(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["CoveoExtension"] = factory();
	else
		root["CoveoExtension"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/js/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	function __export(m) {
	    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
	}
	var _this = this;
	var CustomEvents_1 = __webpack_require__(2);
	exports.CustomEvents = CustomEvents_1.CustomEvents;
	__export(__webpack_require__(3));
	var GatesHelper_1 = __webpack_require__(5);
	exports.GatesHelper = GatesHelper_1.GatesHelper;
	var GatesFacetManager_1 = __webpack_require__(10);
	exports.GatesFacetManager = GatesFacetManager_1.GatesFacetManager;
	var GatesCusto_1 = __webpack_require__(4);
	exports.GatesCusto = GatesCusto_1.GatesCusto;
	var RecommendedResultList_1 = __webpack_require__(12);
	exports.RecommendedResultList = RecommendedResultList_1.RecommendedResultList;
	var GatesResultLayoutCusto_1 = __webpack_require__(11);
	exports.GatesResultLayoutCusto = GatesResultLayoutCusto_1.GatesResultLayoutCusto;
	var GatesHelperSalesforceContact_1 = __webpack_require__(6);
	exports.GatesHelperSalesforceContact = GatesHelperSalesforceContact_1.GatesHelperSalesforceContact;
	var GatesHelperSharepointContact_1 = __webpack_require__(7);
	exports.GatesHelperSharepointContact = GatesHelperSharepointContact_1.GatesHelperSharepointContact;
	var GatesHelperIcon_1 = __webpack_require__(8);
	exports.GatesHelperIcon = GatesHelperIcon_1.GatesHelperIcon;
	var GatesHelperAddress_1 = __webpack_require__(9);
	exports.GatesHelperAddress = GatesHelperAddress_1.GatesHelperAddress;
	// export { GatesHelperSalesforceObjectType } from './templates/GatesHelperSalesforceObjectType';
	// Webpack output a library target with a temporary name.
	// This is to allow end user to put CoveoPSComponents.js before or after the main CoveoJsSearch.js, without breaking
	// This code swap the current module to the "real" Coveo variable.
	var swapVar = function () {
	    if (window['Coveo'] == undefined) {
	        window['Coveo'] = _this;
	    }
	    else {
	        _.each(_.keys(_this), function (k) {
	            window['Coveo'][k] = _this[k];
	        });
	    }
	};
	swapVar();


/***/ }),
/* 2 */
/***/ (function(module, exports) {

	"use strict";
	var CustomEvents = (function () {
	    function CustomEvents() {
	    }
	    CustomEvents.yourCustomEvent = 'yourCustomEvent';
	    return CustomEvents;
	}());
	exports.CustomEvents = CustomEvents;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	var GatesCusto_1 = __webpack_require__(4);
	function initGatesCusto(element, options) {
	    if (options === void 0) { options = {}; }
	    Coveo.Initialization.initializeFramework(element, options, function () {
	        var custo = new GatesCusto_1.GatesCusto(element);
	        custo.initStrings();
	        var customOptions = _.extend(options, custo.getDefaultOptions());
	        Coveo.Initialization.initSearchInterface(element, customOptions);
	    });
	}
	exports.initGatesCusto = initGatesCusto;
	function initGatesAgentBoxCusto(element, options) {
	    if (options === void 0) { options = {}; }
	    Coveo.Initialization.initializeFramework(element, options, function () {
	        var custo = new GatesCusto_1.GatesCusto(element);
	        // custo.initStrings();
	        var customOptions = _.extend(options, custo.getDefaultOptions());
	        Coveo.Initialization.initBoxInterface(element, customOptions);
	    });
	}
	exports.initGatesAgentBoxCusto = initGatesAgentBoxCusto;
	function renewGatesAccessToken() {
	    var renewTokenPromise = new Promise(function (resolve, reject) {
	        var xhr = new XMLHttpRequest();
	        xhr.open('POST', '/renewtoken');
	        xhr.onreadystatechange = handler;
	        xhr.setRequestHeader('Accept', 'application/text');
	        xhr.send();
	        function handler() {
	            if (this.readyState === this.DONE) {
	                if (this.status === 200) {
	                    resolve(this.response);
	                }
	                else {
	                    reject(new Error('renewtoken failed with status: [' + this.status + ']'));
	                }
	            }
	        }
	        ;
	    });
	    return renewTokenPromise;
	}
	exports.renewGatesAccessToken = renewGatesAccessToken;
	Coveo.Initialization.registerNamedMethod('initGatesCusto', function (element, options) {
	    if (options === void 0) { options = {}; }
	    initGatesCusto(element, options);
	});
	Coveo.Initialization.registerNamedMethod('initGatesAgentBoxCusto', function (element, options) {
	    if (options === void 0) { options = {}; }
	    initGatesAgentBoxCusto(element, options);
	});
	Coveo.Initialization.registerNamedMethod('renewGatesAccessToken', function () { renewGatesAccessToken(); });


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	var $$ = Coveo.$$;
	var GatesHelper_1 = __webpack_require__(5);
	var GatesHelperSalesforceContact_1 = __webpack_require__(6);
	var GatesHelperSharepointContact_1 = __webpack_require__(7);
	var GatesHelperIcon_1 = __webpack_require__(8);
	var GatesHelperAddress_1 = __webpack_require__(9);
	var GatesFacetManager_1 = __webpack_require__(10);
	var GatesResultLayoutCusto_1 = __webpack_require__(11);
	/**
	 * Required customization specifically applied for Tableau's implementation
	 */
	var GatesCusto = (function () {
	    function GatesCusto(searchInterfaceElement) {
	        var _this = this;
	        this.searchInterfaceElement = searchInterfaceElement;
	        this.rootElement = $$(searchInterfaceElement);
	        var changeFacetSourceEvtNames = [
	            this.getStateEventName(Coveo.QueryStateModel.eventTypes.changeOne + 'f:allSource'),
	            this.getStateEventName(Coveo.QueryStateModel.eventTypes.changeOne + 'f:coreSource')
	        ];
	        var changeAllFacetsEvtNames = _.map(this.rootElement.findAll('.CoveoFacet'), function (el) {
	            return _this.getStateEventName(Coveo.QueryStateModel.eventTypes.changeOne + 'f:' + el.dataset['id']);
	        });
	        // Initialization Events
	        this.rootElement.on(Coveo.InitializationEvents.beforeInitialization, function () { return _this.handleBeforeInit(); });
	        this.rootElement.on(Coveo.InitializationEvents.afterInitialization, function () { return _this.handleAfterInit(); });
	        this.rootElement.on(Coveo.InitializationEvents.afterComponentsInitialization, function () { return _this.handleAfterComponentsInit(); });
	        // Query Events
	        this.rootElement.on(Coveo.QueryEvents.newQuery, function (e, data) { return _this.handleNewQuery(e, data); });
	        this.rootElement.on(Coveo.QueryEvents.buildingQuery, function (e, data) { return _this.handleBuildingQuery(e, data); });
	        this.rootElement.on(Coveo.QueryEvents.doneBuildingQuery, function (e, data) { return _this.handleDoneBuildingQuery(e, data); });
	        this.rootElement.on(Coveo.QueryEvents.preprocessResults, function (e, data) { return _this.handlePreprocessResults(e, data); });
	        this.rootElement.on(Coveo.QueryEvents.querySuccess, function (e, data) { return _this.handleQuerySuccess(e, data); });
	        this.rootElement.on(Coveo.QueryEvents.queryError, function (e, data) { return _this.handleQueryError(e, data); });
	        this.rootElement.on(Coveo.QueryEvents.noResults, function (e, data) { return _this.handleNoResults(e, data); });
	        // State Events
	        this.rootElement.on(this.getStateEventName(Coveo.QueryStateModel.eventTypes.changeOne + 't'), function (e, data) { return _this.handleTabStateChange(e, data); });
	        // Custom Events
	        // TODO
	        // Custom Facets
	        GatesFacetManager_1.GatesFacetManager.initializeFacetManager();
	    }
	    GatesCusto.prototype.getStateEventName = function (event) {
	        return Coveo.QueryStateModel.ID + ':' + event;
	    };
	    /**
	     * Before Initialization
	     */
	    GatesCusto.prototype.handleBeforeInit = function () {
	    };
	    /**
	     * After Initialization
	     */
	    GatesCusto.prototype.handleAfterInit = function () {
	    };
	    /**
	     * After Component Initialization
	     * Adding Setting Menu item for Tableau training link
	     * Registering custom template helper to manage tableau custom icons. see result templates
	     */
	    GatesCusto.prototype.handleAfterComponentsInit = function () {
	        var _this = this;
	        this.searchInterface = Coveo.Component.get(this.rootElement.el, Coveo.SearchInterface);
	        this.rootElement.on(Coveo.QueryEvents.preprocessResults, function (e, data) { return _this.handlePreprocessResults(e, data); });
	        /**
	         * Register Template Helpers
	         */
	        Coveo.TemplateHelpers.registerTemplateHelper('fromGatesSourceToIcon', function (result, options) {
	            return GatesHelperIcon_1.GatesHelperIcon.fromGatesSourceToIcon(result, options);
	        });
	        Coveo.TemplateHelpers.registerTemplateHelper('fromGatesContactTypeToIcon', function (result, options) {
	            return GatesHelperIcon_1.GatesHelperIcon.fromGatesContactTypeToIcon(result, options);
	        });
	        Coveo.TemplateHelpers.registerTemplateHelper('fromGatesFileTypeToIcon', function (result, options) {
	            return GatesHelperIcon_1.GatesHelperIcon.fromGatesFileTypeToIcon(result, options);
	        });
	        Coveo.TemplateHelpers.registerFieldHelper('customDate', function (value, options) {
	            return GatesHelper_1.GatesHelper.customDate(Coveo.DateUtils.convertFromJsonDateIfNeeded(value));
	        });
	        // Coveo.TemplateHelpers.registerTemplateHelper('getContactInformation', (result: Coveo.IQueryResult) => {
	        //   return GatesHelperContact.getContactInformation(result);
	        // });
	        Coveo.TemplateHelpers.registerTemplateHelper('fromSalesforceObjectTypeToIcon', function (result, options) {
	            return GatesHelperIcon_1.GatesHelperIcon.fromSalesforceObjectTypeToIcon(result, options);
	        });
	        Coveo.TemplateHelpers.registerFieldHelper('customAddress', function (value, options) {
	            return GatesHelperAddress_1.GatesHelperAddress.customAddress(value);
	        });
	        /**
	         * SalesforceContact.ejs customization
	         */
	        Coveo.TemplateHelpers.registerFieldHelper('sfCustomContactType', function (value, options) {
	            return GatesHelperSalesforceContact_1.GatesHelperSalesforceContact.getCustomContactType(value);
	        });
	        Coveo.TemplateHelpers.registerFieldHelper('sfCustomAccountName', function (value, options) {
	            return GatesHelperSalesforceContact_1.GatesHelperSalesforceContact.getCustomAccountName(value);
	        });
	        Coveo.TemplateHelpers.registerFieldHelper('sfCustomMobilePhone', function (value, options) {
	            return GatesHelperSalesforceContact_1.GatesHelperSalesforceContact.getCustomMobilePhone(value, options.doNotPhone);
	        });
	        Coveo.TemplateHelpers.registerFieldHelper('sfCustomPhone', function (value, options) {
	            return GatesHelperSalesforceContact_1.GatesHelperSalesforceContact.getCustomPhone(value, options.doNotPhone);
	        });
	        Coveo.TemplateHelpers.registerFieldHelper('sfCustomEmail', function (value, options) {
	            return GatesHelperSalesforceContact_1.GatesHelperSalesforceContact.getCustomEmail(value, options.doNotEmail);
	        });
	        Coveo.TemplateHelpers.registerFieldHelper('sfCustomMailingAddress', function (value, options) {
	            return GatesHelperSalesforceContact_1.GatesHelperSalesforceContact.getCustomMailingAddress(JSON.parse(value));
	        });
	        /**
	         * SharepointContact.ejs customization
	         */
	        Coveo.TemplateHelpers.registerFieldHelper('spCustomJobTitle', function (value, options) {
	            return GatesHelperSharepointContact_1.GatesHelperSharepointContact.getCustomJobTitle(value);
	        });
	        Coveo.TemplateHelpers.registerFieldHelper('spCustomCompany', function (value, options) {
	            return GatesHelperSharepointContact_1.GatesHelperSharepointContact.getCustomCompany(value);
	        });
	        Coveo.TemplateHelpers.registerFieldHelper('spCustomMobilePhone', function (value, options) {
	            return GatesHelperSharepointContact_1.GatesHelperSharepointContact.getCustomMobilePhone(value);
	        });
	        Coveo.TemplateHelpers.registerFieldHelper('spCustomPhone', function (value, options) {
	            return GatesHelperSharepointContact_1.GatesHelperSharepointContact.getCustomPhone(value);
	        });
	        Coveo.TemplateHelpers.registerFieldHelper('spCustomEmail', function (value, options) {
	            return GatesHelperSharepointContact_1.GatesHelperSharepointContact.getCustomEmail(value);
	        });
	        Coveo.TemplateHelpers.registerFieldHelper('spCustomMailingAddress', function (value, options) {
	            return GatesHelperSharepointContact_1.GatesHelperSharepointContact.getCustomMailingAddress(JSON.parse(value));
	        });
	    };
	    /**
	     * New Query
	     */
	    GatesCusto.prototype.handleNewQuery = function (evt, args) {
	    };
	    /**
	     * Building Query
	     */
	    GatesCusto.prototype.handleBuildingQuery = function (evt, args) { };
	    /**
	     * Done Building Query
	     */
	    GatesCusto.prototype.handleDoneBuildingQuery = function (evt, args) { };
	    /**
	     * Preprocess Results
	     */
	    GatesCusto.prototype.handlePreprocessResults = function (evt, args) {
	        _.each(args.results.results, function (result) {
	            //debugger;
	            // Add web =1 parameter for SharePoint content (http://www.evokeit.com/making-a-link-to-a-document-in-sharepoint-2013-open-in-browser/)
	            if (result.clickUri.indexOf('web=1') < 0 &&
	                result.raw.commonsource == 'SharePoint' &&
	                result.raw.sysfiletype != 'spdocumentlibrarylist' &&
	                result.raw.sysfiletype != 'spsite' &&
	                (result.raw.filename && result.raw.filename.indexOf('.aspx') < 0) &&
	                result.raw.owsid) {
	                result.clickUri = result.clickUri + '?web=1';
	            }
	            // MONKEY PATCH: show SharePoint spunknownlist results as mp4 files
	            if (result.raw.commonsource == 'SharePoint' &&
	                (result.raw.filetype && result.raw.filetype == 'spunknownlist') &&
	                (result.raw.filename && result.raw.filename.indexOf('.mp4') >= 0)) {
	                result.raw.filetype = 'mp4';
	            }
	        });
	    };
	    /**
	     * Query Success
	     */
	    GatesCusto.prototype.handleQuerySuccess = function (evt, args) { };
	    /**
	     * Query Error
	     */
	    GatesCusto.prototype.handleQueryError = function (evt, args) {
	        // if token has expired , business wants to force a logout.
	        // if(args.error['status'] === 419) {
	        //   window.location.href = '/logout';
	        // }
	    };
	    /**
	     * No Results
	     */
	    GatesCusto.prototype.handleNoResults = function (evt, args) {
	    };
	    /**
	     * Tab State Change
	     */
	    GatesCusto.prototype.handleTabStateChange = function (evt, args) {
	        GatesResultLayoutCusto_1.GatesResultLayoutCusto.handleActiveTab(this.rootElement, args.value);
	    };
	    /**
	     * Initialized Custom Strings for Tableau
	     */
	    GatesCusto.prototype.initStrings = function () {
	        // Custom variable for current application
	        String.toLocaleString({
	            'en': {
	                'ShowingResultsOf': 'Result<pl>s</pl> {0}<pl>-{1}</pl> of {2}',
	                'RemoveContext': 'Remove Case Filters',
	                'GoToFullSearch': 'Full Search Page',
	                'Relevance': 'most relevant',
	                'Date': 'most recent',
	                'FacetHeaderEraser': 'clear',
	                'FacetFooterEraser': 'clear',
	                'ShowMore': 'more',
	                'ShowLess': 'less',
	                'RecommendedResultListTitle': 'Recommended Results',
	                'NA': 'N/A'
	            }
	        });
	    };
	    /**
	     * Set default options of different UI Components for Gates.
	     */
	    GatesCusto.prototype.getDefaultOptions = function () {
	        var defaultOptions = {
	            SalesforceResultLink: {
	                alwaysOpenInNewWindow: true
	            },
	            ResultLink: {
	                alwaysOpenInNewWindow: true
	            },
	            Facet: {
	                availableSorts: ['occurrences', 'score', 'alphaAscending', 'alphaDescending'],
	                valueCaption: {
	                    'Not Specified': 'Unspecified',
	                    'Web Misc.': 'Web (other)',
	                    'Spreadsheet Document': 'Spreadsheet',
	                    'Presentation Document': 'Presentation',
	                    'HTML File': 'HTML',
	                    'msg': 'Message',
	                    'mime': 'MIME',
	                    'Giving_Pledge_Events__c': 'Event',
	                    'Others': 'Other'
	                }
	            }
	        };
	        return defaultOptions;
	    };
	    return GatesCusto;
	}());
	exports.GatesCusto = GatesCusto;
	;


/***/ }),
/* 5 */
/***/ (function(module, exports) {

	"use strict";
	var IGatesIconOptions = (function () {
	    function IGatesIconOptions() {
	        this.value = null;
	        this.additionalClass = null;
	    }
	    return IGatesIconOptions;
	}());
	exports.IGatesIconOptions = IGatesIconOptions;
	var GatesHelper = (function () {
	    function GatesHelper() {
	    }
	    GatesHelper.customDate = function (d) {
	        var dateOnly = Coveo.DateUtils.keepOnlyDatePart(d);
	        var today = Coveo.DateUtils.keepOnlyDatePart(new Date());
	        var options = {};
	        if (dateOnly.getFullYear() === today.getFullYear()) {
	            options.predefinedFormat = 'MMM dd';
	        }
	        else {
	            options.predefinedFormat = 'MMM dd, yyyy';
	        }
	        return Coveo.DateUtils.dateToString(d, options);
	    };
	    GatesHelper.KBExternalLink = function (result, options) {
	        var kbExternalUrl = '';
	        var kbArticleType = result.raw.sfkbarticletype || result.raw.sfknowledgebasearticletype || '';
	        var lang = result.raw.sflanguage || '';
	        if (kbArticleType) {
	            kbArticleType = kbArticleType.replace(/__kav/i, '');
	            var kbUrlName = result.raw.sfkburlname || result.raw.sfknowledgebaseurlname || '';
	            var urlPath = kbArticleType + '/' + kbUrlName;
	            lang = lang.substring(0, 2) || lang;
	            urlPath = result.raw.commonlanguage.toLowerCase() == 'english' ? urlPath : (urlPath + '?lang=' + lang);
	            kbExternalUrl = kbUrlName ? (options.baseUrl + urlPath) : result.raw.clickUri;
	        }
	        return kbExternalUrl;
	    };
	    GatesHelper.kbExternalLinkOptions = {
	        baseUrl: 'http://kb.Gates.com/articles/'
	    };
	    return GatesHelper;
	}());
	exports.GatesHelper = GatesHelper;


/***/ }),
/* 6 */
/***/ (function(module, exports) {

	"use strict";
	var GatesHelperSalesforceContact = (function () {
	    function GatesHelperSalesforceContact() {
	    }
	    GatesHelperSalesforceContact.getCustomContactType = function (contactType) {
	        var containerSpanDom = Coveo.$$('span');
	        var contactTypeContainerDom = Coveo.$$('div', { className: 'contact-card__contact-type' }, Coveo.$$('span', { className: 'contact-card__info' }, contactType));
	        containerSpanDom.append(contactTypeContainerDom.el);
	        return containerSpanDom.el.innerHTML;
	    };
	    GatesHelperSalesforceContact.getCustomAccountName = function (accountName) {
	        var containerSpanDom = Coveo.$$('span');
	        var accountNameDom = Coveo.$$('div', { className: 'contact-card__account-name' }, Coveo.$$('span', { className: 'contact-card__info' }, accountName));
	        containerSpanDom.append(accountNameDom.el);
	        return containerSpanDom.el.innerHTML;
	    };
	    GatesHelperSalesforceContact.getCustomMobilePhone = function (mobilePhone, doNotPhone) {
	        // we might want to display an icon if @sfdonotphonec=true
	        var containerSpanDom = Coveo.$$('span');
	        var mobilePhoneIconContainerDom = Coveo.$$('div', { className: 'card-item-icon-container' });
	        var mobilePhoneNumberContainerDom = Coveo.$$('div', { className: 'card-item-data-container' });
	        var mobilePhoneIconDom = Coveo.$$('i', { className: 'fa fa-mobile', 'aria-hidden': true });
	        var mobilePhoneDom = Coveo.$$('span', { className: 'contact-card__mobile-phone' }, Coveo.$$('span', { className: 'contact-card__info' }, mobilePhone));
	        mobilePhoneIconContainerDom.append(mobilePhoneIconDom.el);
	        mobilePhoneNumberContainerDom.append(mobilePhoneDom.el);
	        containerSpanDom.append(mobilePhoneIconContainerDom.el);
	        containerSpanDom.append(mobilePhoneNumberContainerDom.el);
	        // mobilePhoneDom.prepend(mobilePhoneIconDom.el);
	        if (doNotPhone == 'true') {
	            var doNotContactIconDom = Coveo.$$('i', { className: 'fa fa-ban', 'aria-hidden': true });
	            mobilePhoneDom.append(doNotContactIconDom.el);
	        }
	        return containerSpanDom.el.innerHTML;
	    };
	    GatesHelperSalesforceContact.getCustomPhone = function (phone, doNotPhone) {
	        // we might want to display an icon if @sfdonotphonec=true
	        var containerSpanDom = Coveo.$$('span');
	        var phoneIconContainerDom = Coveo.$$('div', { className: 'card-item-icon-container' });
	        var phoneNumberContainerDom = Coveo.$$('div', { className: 'card-item-data-container' });
	        var phoneIconDom = Coveo.$$('i', { className: 'fa fa-phone', 'aria-hidden': true });
	        var phoneDom = Coveo.$$('span', { className: 'contact-card__phone' }, Coveo.$$('span', { className: 'contact-card__info' }, phone));
	        phoneIconContainerDom.append(phoneIconDom.el);
	        phoneNumberContainerDom.append(phoneDom.el);
	        containerSpanDom.append(phoneIconContainerDom.el);
	        containerSpanDom.append(phoneNumberContainerDom.el);
	        // phoneDom.prepend(phoneIconDom.el);
	        if (doNotPhone == 'true') {
	            var doNotContactIconDom = Coveo.$$('i', { className: 'fa fa-ban', 'aria-hidden': true });
	            phoneDom.append(doNotContactIconDom.el);
	        }
	        return containerSpanDom.el.innerHTML;
	    };
	    GatesHelperSalesforceContact.getCustomEmail = function (mailingAddress, doNotEmail) {
	        var containerSpanDom = Coveo.$$('span');
	        var emailIconContainerDom = Coveo.$$('div', { className: 'card-item-icon-container' });
	        var emailContainerDom = Coveo.$$('div', { className: 'card-item-data-container' });
	        // we might want to display an icon if @sfdonotemailc=true
	        var emailIconDom = Coveo.$$('i', { className: 'fa fa-envelope-o', 'aria-hidden': true });
	        var emailDom = Coveo.$$('span', { className: 'contact-card__email' }, Coveo.$$('span', { className: 'contact-card__info' }, mailingAddress));
	        emailIconContainerDom.append(emailIconDom.el);
	        emailContainerDom.append(emailDom.el);
	        containerSpanDom.append(emailIconContainerDom.el);
	        containerSpanDom.append(emailContainerDom.el);
	        if (doNotEmail == 'true') {
	            var doNotContactIconDom = Coveo.$$('i', { className: 'fa fa-ban', 'aria-hidden': true });
	            emailDom.append(doNotContactIconDom.el);
	        }
	        return containerSpanDom.el.innerHTML;
	    };
	    GatesHelperSalesforceContact.getCustomMailingAddress = function (mailingAddress) {
	        var containerSpan = Coveo.$$('span');
	        var mailingAddressIconContainerDom = Coveo.$$('div', { className: 'card-item-icon-container' });
	        var mailingAddressContainerDom = Coveo.$$('div', { className: 'card-item-data-container' });
	        var mailingAddressIconDom = Coveo.$$('i', { className: 'fa fa-map-marker', 'aria-hidden': true });
	        var mailingAddressFormated = '';
	        if (!_.isEmpty(mailingAddress)) {
	            mailingAddressFormated = "<span class=\"mailing-adddress_line01\">" + (mailingAddress.street || '') + "</span>\n                                      <span class=\"mailing-adddress_line02\">" + (mailingAddress.city || '') + " " + (mailingAddress.stateCode || '') + " " + (mailingAddress.postalCode || '') + "</span>\n                                      <span class=\"mailing-adddress_line03\">" + (mailingAddress.countryCode || '') + "</span>\n                                     ";
	        }
	        else {
	            mailingAddressFormated = Coveo.l('NA');
	        }
	        var mailingAddressDom = Coveo.$$('span', { className: 'contact-card__mailing-adddress' }, Coveo.$$('span', { className: 'contact-card__info' }, mailingAddressFormated));
	        mailingAddressIconContainerDom.append(mailingAddressIconDom.el);
	        mailingAddressContainerDom.append(mailingAddressDom.el);
	        containerSpan.append(mailingAddressIconContainerDom.el);
	        containerSpan.append(mailingAddressContainerDom.el);
	        // mailingAddressDom.prepend(mailingAddressIconDom.el);
	        return containerSpan.el.innerHTML;
	    };
	    return GatesHelperSalesforceContact;
	}());
	exports.GatesHelperSalesforceContact = GatesHelperSalesforceContact;


/***/ }),
/* 7 */
/***/ (function(module, exports) {

	"use strict";
	var GatesHelperSharepointContact = (function () {
	    function GatesHelperSharepointContact() {
	    }
	    GatesHelperSharepointContact.getCustomJobTitle = function (contactType) {
	        var containerSpanDom = Coveo.$$('span');
	        var contactTypeContainerDom = Coveo.$$('div', { className: 'contact-card__contact-type' }, Coveo.$$('span', { className: 'contact-card__info' }, contactType));
	        containerSpanDom.append(contactTypeContainerDom.el);
	        return containerSpanDom.el.innerHTML;
	    };
	    GatesHelperSharepointContact.getCustomCompany = function (accountName) {
	        var containerSpanDom = Coveo.$$('span');
	        var accountNameDom = Coveo.$$('div', { className: 'contact-card__account-name' }, Coveo.$$('span', { className: 'contact-card__info' }, accountName));
	        containerSpanDom.append(accountNameDom.el);
	        return containerSpanDom.el.innerHTML;
	    };
	    GatesHelperSharepointContact.getCustomMobilePhone = function (mobilePhone) {
	        // we might want to display an icon if @sfdonotphonec=true
	        var containerSpanDom = Coveo.$$('span');
	        var mobilePhoneIconContainerDom = Coveo.$$('div', { className: 'card-item-icon-container' });
	        var mobilePhoneNumberContainerDom = Coveo.$$('div', { className: 'card-item-data-container' });
	        var mobilePhoneIconDom = Coveo.$$('i', { className: 'fa fa-mobile', 'aria-hidden': true });
	        var mobilePhoneDom = Coveo.$$('span', { className: 'contact-card__mobile-phone' }, Coveo.$$('span', { className: 'contact-card__info' }, mobilePhone));
	        mobilePhoneIconContainerDom.append(mobilePhoneIconDom.el);
	        mobilePhoneNumberContainerDom.append(mobilePhoneDom.el);
	        containerSpanDom.append(mobilePhoneIconContainerDom.el);
	        containerSpanDom.append(mobilePhoneNumberContainerDom.el);
	        return containerSpanDom.el.innerHTML;
	    };
	    GatesHelperSharepointContact.getCustomPhone = function (phone) {
	        // we might want to display an icon if @sfdonotphonec=true
	        var containerSpanDom = Coveo.$$('span');
	        var phoneIconContainerDom = Coveo.$$('div', { className: 'card-item-icon-container' });
	        var phoneNumberContainerDom = Coveo.$$('div', { className: 'card-item-data-container' });
	        var phoneIconDom = Coveo.$$('i', { className: 'fa fa-phone', 'aria-hidden': true });
	        var phoneDom = Coveo.$$('span', { className: 'contact-card__phone' }, Coveo.$$('span', { className: 'contact-card__info' }, phone));
	        phoneIconContainerDom.append(phoneIconDom.el);
	        phoneNumberContainerDom.append(phoneDom.el);
	        containerSpanDom.append(phoneIconContainerDom.el);
	        containerSpanDom.append(phoneNumberContainerDom.el);
	        return containerSpanDom.el.innerHTML;
	    };
	    GatesHelperSharepointContact.getCustomEmail = function (mailingAddress) {
	        var containerSpanDom = Coveo.$$('span');
	        var emailIconContainerDom = Coveo.$$('div', { className: 'card-item-icon-container' });
	        var emailContainerDom = Coveo.$$('div', { className: 'card-item-data-container' });
	        // we might want to display an icon if @sfdonotemailc=true
	        var emailIconDom = Coveo.$$('i', { className: 'fa fa-envelope-o', 'aria-hidden': true });
	        var emailDom = Coveo.$$('span', { className: 'contact-card__email' }, Coveo.$$('span', { className: 'contact-card__info' }, mailingAddress));
	        emailIconContainerDom.append(emailIconDom.el);
	        emailContainerDom.append(emailDom.el);
	        containerSpanDom.append(emailIconContainerDom.el);
	        containerSpanDom.append(emailContainerDom.el);
	        return containerSpanDom.el.innerHTML;
	    };
	    GatesHelperSharepointContact.getCustomMailingAddress = function (mailingAddress) {
	        var containerSpan = Coveo.$$('span');
	        if (mailingAddress.street || mailingAddress.city || mailingAddress.state || mailingAddress.postalCode || mailingAddress.country) {
	            var mailingAddressIconContainerDom = Coveo.$$('div', { className: 'card-item-icon-container' });
	            var mailingAddressContainerDom = Coveo.$$('div', { className: 'card-item-data-container' });
	            var mailingAddressIconDom = Coveo.$$('i', { className: 'fa fa-map-marker', 'aria-hidden': true });
	            var mailingAddressFormated = '';
	            if (!_.isEmpty(mailingAddress)) {
	                mailingAddressFormated = "<span class=\"mailing-adddress_line01\">" + (mailingAddress.street || '') + "</span>\n                                        <span class=\"mailing-adddress_line02\">" + (mailingAddress.city || '') + " " + (mailingAddress.state || '') + " " + (mailingAddress.postalCode || '') + "</span>\n                                        <span class=\"mailing-adddress_line03\">" + (mailingAddress.country || '') + "</span>\n                                      ";
	            }
	            else {
	                mailingAddressFormated = Coveo.l('NA');
	            }
	            var mailingAddressDom = Coveo.$$('span', { className: 'contact-card__mailing-adddress' }, Coveo.$$('span', { className: 'contact-card__info' }, mailingAddressFormated));
	            mailingAddressIconContainerDom.append(mailingAddressIconDom.el);
	            mailingAddressContainerDom.append(mailingAddressDom.el);
	            containerSpan.append(mailingAddressIconContainerDom.el);
	            containerSpan.append(mailingAddressContainerDom.el);
	        }
	        return containerSpan.el.innerHTML;
	    };
	    return GatesHelperSharepointContact;
	}());
	exports.GatesHelperSharepointContact = GatesHelperSharepointContact;


/***/ }),
/* 8 */
/***/ (function(module, exports) {

	"use strict";
	var OBJECT_TYPE_ICONS_MAPPING = {
	    'unknown': 'CoveoIcon',
	    'account': 'account',
	    'campaign': 'campaign',
	    'lead': 'lead'
	};
	var CONTACT_TYPE_ICONS_MAPPING = {
	    'unknown': 'CoveoIcon',
	    'contact': 'ContactAvatar',
	    'spcontact': 'ContactAvatar'
	};
	var FILE_TYPE_ICONS_MAPPING = {
	    // Media
	    'swf': 'swf',
	    'mp4': 'mp4',
	    'mp3': 'mp3',
	    'wav': 'wav',
	    'mpg': 'mpg',
	    'wmv': 'wmv',
	    'mov': 'mov',
	    'wma': 'wma',
	    'm4v': 'm4v',
	    'wvx': 'wvx',
	    'm4a': 'm4a',
	    // Documents
	    'dwg': 'dwg',
	    'dwp': 'dwp',
	    'xls': 'xls',
	    'xlsb': 'xlsb',
	    'xlsx': 'xlsx',
	    'ttf': 'ttf',
	    'woff': 'woff',
	    'woff2': 'woff2',
	    'image': 'image',
	    'svg': 'svg',
	    'ico': 'ico',
	    'eps': 'eps',
	    'xml': 'xml',
	    'xsd': 'xsd',
	    'one': 'one',
	    'onetoc2': 'onetoc2',
	    'onebin': 'onebin',
	    'onepkg': 'onepkg',
	    'msg': 'msg',
	    'pdf': 'pdf',
	    'xps': 'xps',
	    'ppt': 'ppt',
	    'pptx': 'pptx',
	    'mpp': 'mpp',
	    'pub': 'pub',
	    'twb': 'twb',
	    'vsd': 'vsd',
	    'html': 'html',
	    'htm': 'htm',
	    'doc': 'doc',
	    'docx': 'docx',
	    'rtf': 'rtf',
	    'zip': 'zip',
	    'gz': 'gz',
	    'rar': 'rar',
	    'tar': 'tar',
	    'txt': 'txt',
	    'unknow': 'unknown'
	};
	var FILE_TYPE_SPRITE_MAPPING = {
	    'spevent': 'standard',
	    'spannouncement': 'standard',
	    'sptask': 'standard',
	    'spprojecttask': 'standard',
	    'spblogpost': 'standard',
	    'unknown': 'unknown'
	};
	var SOURCE_ICONS_MAPPING = {
	    'unknown': 'CoveoIcon'
	};
	var IGatesHelperIconOptions = (function () {
	    function IGatesHelperIconOptions() {
	        this.value = null;
	        this.additionalClass = null;
	    }
	    return IGatesHelperIconOptions;
	}());
	exports.IGatesHelperIconOptions = IGatesHelperIconOptions;
	var GatesHelperIcon = (function () {
	    function GatesHelperIcon() {
	    }
	    GatesHelperIcon.fromSalesforceObjectTypeToIcon = function (result, options) {
	        var iconCss = options.value;
	        var objectType = (result.raw.objecttype || '').toLowerCase();
	        if (Coveo.Utils.isNullOrEmptyString(iconCss)) {
	            iconCss = OBJECT_TYPE_ICONS_MAPPING[objectType || 'unknown'] || 'unknown';
	            iconCss = !Coveo.Utils.isNullOrEmptyString(options.additionalClass) ? (iconCss + ' ' + options.additionalClass) : iconCss;
	        }
	        return Coveo.$$('div', {
	            className: 'bmgf-icon_container bmgf-icon-' + iconCss,
	            title: objectType
	        }).el.outerHTML;
	    };
	    GatesHelperIcon.fromGatesFileTypeToIcon = function (result, options) {
	        var fileType;
	        // Monkey patch to have the web icon for document containing '.aspx' in there filename.
	        if (result.raw.filename && result.raw.filename.indexOf('.aspx') >= 0) {
	            fileType = 'html';
	        }
	        else if (result.raw.filetype && result.raw.filetype == 'spdocumentset') {
	            fileType = 'html';
	        }
	        else {
	            fileType = (result.raw.filetype || '').toLowerCase().replace('.', '');
	        }
	        var iconCss = options.value;
	        if (Coveo.Utils.isNullOrEmptyString(iconCss)) {
	            iconCss = FILE_TYPE_ICONS_MAPPING[fileType || 'unknown'] || 'unknown';
	            iconCss = !Coveo.Utils.isNullOrEmptyString(options.additionalClass) ? (iconCss + ' ' + options.additionalClass) : iconCss;
	        }
	        return Coveo.$$('div', {
	            className: 'bmgf-icon_container bmgf-icon-' + iconCss,
	            title: fileType
	        }).el.outerHTML;
	    };
	    GatesHelperIcon.fromGatesSourceToIcon = function (result, options) {
	        var iconCss = options.value;
	        var hoverLabel = result.raw.commonsource || '';
	        if (Coveo.Utils.isNullOrEmptyString(iconCss)) {
	            var commonSrc = (result.raw.commonsource || '').toLowerCase();
	            iconCss = SOURCE_ICONS_MAPPING[commonSrc.toLowerCase()] || SOURCE_ICONS_MAPPING['unknown'];
	            if (commonSrc === 'knowledge base') {
	                iconCss = GatesHelperIcon.isAPublicKB(result) ? 'icon-external-knowledge-base' : 'icon-internal-knowledge-base';
	                hoverLabel = GatesHelperIcon.isAPublicKB(result) ? 'External Knowledge Base' : 'Internal Knowledge Base';
	            }
	            else if (commonSrc === 'community') {
	                var question = result.raw.jivequestion ? result.raw.jivequestion.toLowerCase() : '';
	                iconCss = (question === 'true' && result.raw.jiveanswer != null) ? 'icon-community-discussion-answered' : iconCss;
	                iconCss = (question === 'true' && (!result.raw.jiveanswer || result.raw.jiveanswer === 'False')) ? 'icon-community-discussion-unanswered' : iconCss;
	            }
	            iconCss = !Coveo.Utils.isNullOrEmptyString(options.additionalClass) ? (iconCss + ' ' + options.additionalClass) : iconCss;
	        }
	        return Coveo.$$('i', {
	            className: iconCss,
	            title: hoverLabel,
	            'aria-hidden': true
	        }).el.outerHTML;
	    };
	    GatesHelperIcon.fromGatesContactTypeToIcon = function (result, options) {
	        var iconCss = options.value;
	        var hoverLabel = result.raw.objecttype || '';
	        var contact_initials = '';
	        if (Coveo.Utils.isNullOrEmptyString(iconCss)) {
	            var commonSourceType = result.raw.commonsource;
	            var documentType = '';
	            switch (commonSourceType) {
	                case 'SharePoint': {
	                    documentType = (result.raw.filetype || '').toLowerCase();
	                    break;
	                }
	                case 'Salesforce': {
	                    documentType = (result.raw.objecttype || '').toLowerCase();
	                    break;
	                }
	            }
	            iconCss = CONTACT_TYPE_ICONS_MAPPING[documentType] || CONTACT_TYPE_ICONS_MAPPING['unknown'];
	            if (documentType === 'contact') {
	                var firstname = result.raw.firstname || '';
	                var lastname = result.raw.lastname || '';
	                contact_initials = firstname.charAt(0) + lastname.charAt(0);
	            }
	            else if (documentType === 'spcontact') {
	                var nameArray = result.raw.title.match(/\S+/g) || [];
	                var firstname = nameArray[0] || '';
	                var lastname = nameArray[1] || '';
	                contact_initials = firstname.charAt(0) + lastname.charAt(0);
	            }
	            iconCss = !Coveo.Utils.isNullOrEmptyString(options.additionalClass) ? (iconCss + ' ' + options.additionalClass) : iconCss;
	            iconCss = iconCss + ' avatar-' + GatesHelperIcon.getRandomInt(1, 4);
	        }
	        return Coveo.$$('span', {
	            className: iconCss,
	            title: hoverLabel,
	            'aria-hidden': true,
	            'data-contact-initials': contact_initials.toUpperCase()
	        }).el.outerHTML;
	    };
	    // Util functions
	    GatesHelperIcon.getRandomInt = function (min, max) {
	        min = Math.ceil(min);
	        max = Math.floor(max);
	        return Math.floor(Math.random() * (max - min + 1)) + min;
	    };
	    GatesHelperIcon.isAPublicKB = function (result) {
	        var retVal = false;
	        if (result.raw.sfkbid && result.raw.sfkbisvisibleinpkb === 'True') {
	            retVal = true;
	        }
	        if (result.raw.sfkbid && result.raw.sfknowledgebaseisvisibleinpkb === 'True') {
	            retVal = true;
	        }
	        return retVal;
	    };
	    return GatesHelperIcon;
	}());
	exports.GatesHelperIcon = GatesHelperIcon;


/***/ }),
/* 9 */
/***/ (function(module, exports) {

	"use strict";
	var GatesHelperAddress = (function () {
	    function GatesHelperAddress() {
	    }
	    GatesHelperAddress.customAddress = function (address) {
	        var addressDom = this.getAddress(JSON.parse(address || '{}'));
	        return addressDom;
	    };
	    GatesHelperAddress.getAddress = function (mailingAddress) {
	        var mailingAddressFormated = '';
	        if (!_.isEmpty(mailingAddress)) {
	            mailingAddressFormated =
	                this.formatAddressLigne((mailingAddress.street || '')) +
	                    this.formatAddressLigne((mailingAddress.city || '') + ' ' +
	                        (mailingAddress.stateCode || '') + ' ' +
	                        (mailingAddress.postalCode || '')) +
	                    (mailingAddress.countryCode || '');
	        }
	        else {
	            mailingAddressFormated = Coveo.l('NA');
	        }
	        return mailingAddressFormated;
	    };
	    GatesHelperAddress.formatAddressLigne = function (addressLigne) {
	        var formatAddressLigne = '';
	        if (addressLigne.trim() != '') {
	            formatAddressLigne = addressLigne + '<br />';
	        }
	        return formatAddressLigne;
	    };
	    return GatesHelperAddress;
	}());
	exports.GatesHelperAddress = GatesHelperAddress;


/***/ }),
/* 10 */
/***/ (function(module, exports) {

	"use strict";
	var GatesFacetManager = (function () {
	    function GatesFacetManager() {
	    }
	    /**
	     * Add the boolean option `autoCollapse` on the `Facet` component
	     * Override `createDom` prototype on the `Facet` component
	     */
	    GatesFacetManager.initializeFacetManager = function () {
	        var defaultFacetCreateDom = Coveo.Facet.prototype.createDom;
	        Coveo.Facet.options['autoCollapse'] = Coveo.ComponentOptions.buildBooleanOption({ defaultValue: false });
	        Coveo.Facet.options['actionPositionCollapse'] = Coveo.ComponentOptions.buildStringOption({ defaultValue: 'top-right' });
	        Coveo.Facet.options['actionPositionMoreLess'] = Coveo.ComponentOptions.buildStringOption({ defaultValue: 'align' });
	        Coveo.Facet.prototype.createDom = function () {
	            var self = this;
	            defaultFacetCreateDom.call(self);
	            GatesFacetManager.autoCollapse(self);
	            GatesFacetManager.setActionPositionCollapse(self);
	            GatesFacetManager.setActionPositionMoreLess(self);
	            GatesFacetManager.bindCollapseToggleEvent(self);
	            GatesFacetManager.displayMoreLessLabels(self);
	            GatesFacetManager.displayClearLabels(self);
	        };
	    };
	    /**
	     * Set the action position for the collapse/expand of the `Facet` component
	     * @param facet
	     */
	    GatesFacetManager.setActionPositionCollapse = function (facet) {
	        Coveo.$$(facet.element).addClass('action-position-collapse-' + facet.options['actionPositionCollapse']);
	    };
	    /**
	     * Set the action position for the show more/less of the `Facet` component
	     * @param facet
	     */
	    GatesFacetManager.setActionPositionMoreLess = function (facet) {
	        Coveo.$$(facet.element).addClass('action-position-more-less-' + facet.options['actionPositionMoreLess']);
	    };
	    /**
	     * Collapse the `Facet` if the option `autoCollapse` is set to `true`
	     * @param facet
	     */
	    GatesFacetManager.autoCollapse = function (facet) {
	        if (facet.options['autoCollapse']) {
	            facet.collapse();
	        }
	    };
	    /**
	     * Bind the `click` event on the `Facet` title to toggle between expand and collapse action
	     * @param facet
	     */
	    GatesFacetManager.bindCollapseToggleEvent = function (facet) {
	        var facetToggleElement;
	        if (facet.options['actionPositionCollapse'] == 'top-left') {
	            facetToggleElement = Coveo.$$(facet.element).findClass('coveo-facet-header-title-section')[0];
	        }
	        else if (facet.options['actionPositionCollapse'] == 'top-right') {
	            facetToggleElement = Coveo.$$(facet.element).findClass('coveo-facet-header')[0];
	        }
	        Coveo.$$(facetToggleElement).on('click', function () {
	            if (Coveo.$$(facet.element).hasClass('coveo-facet-collapsed')) {
	                facet.expand();
	            }
	            else {
	                facet.collapse();
	            }
	        });
	    };
	    /**
	     * Display `ShowMore` and `ShowLess` labels instead of icons
	     * @param facet
	     */
	    GatesFacetManager.displayMoreLessLabels = function (facet) {
	        if (facet.options['actionPositionMoreLess'] == 'align') {
	            var facetLessElement = Coveo.$$(facet.element).findClass('coveo-facet-less')[0];
	            var facetMoreElement = Coveo.$$(facet.element).findClass('coveo-facet-more')[0];
	            var newFacetLessButtonElement = Coveo.$$('span', { class: 'coveo-text' }, Coveo.l('ShowLess')).el;
	            var newFacetMoreButtonElement = Coveo.$$('span', { class: 'coveo-text' }, Coveo.l('ShowMore')).el;
	            var newFacetMoreLessSeperator = Coveo.$$('span', { class: 'coveo-more-less-seperator' }, Coveo.l('|')).el;
	            // remove HTMLElement instead of hiding it with CSS
	            Coveo.$$(facetLessElement).findClass('coveo-icon')[0].remove();
	            // remove HTMLElement instead of hiding it with CSS
	            Coveo.$$(facetMoreElement).findClass('coveo-icon')[0].remove();
	            Coveo.$$(facetLessElement).append(newFacetLessButtonElement);
	            Coveo.$$(facetMoreElement).append(newFacetMoreButtonElement);
	            Coveo.$$(facet.element).find('.coveo-facet-footer').appendChild(newFacetMoreLessSeperator);
	        }
	    };
	    /**
	     * Display `FacetHeaderEraser` label instead of an icon
	     * @param facet
	     */
	    GatesFacetManager.displayClearLabels = function (facet) {
	        var facetHeaderEraserElement = Coveo.$$(facet.element).findClass('coveo-facet-footer')[0];
	        var newFacetHeaderEraserElement = Coveo.$$('span', {
	            class: 'coveo-text coveo-footer-eraser',
	        }, Coveo.l('FacetHeaderEraser')).el;
	        Coveo.$$(facetHeaderEraserElement).append(newFacetHeaderEraserElement);
	        Coveo.$$(newFacetHeaderEraserElement).on('click', function () {
	            facet.reset();
	            Coveo.executeQuery(facet.root);
	        });
	    };
	    return GatesFacetManager;
	}());
	exports.GatesFacetManager = GatesFacetManager;


/***/ }),
/* 11 */
/***/ (function(module, exports) {

	"use strict";
	var GatesResultLayoutCusto = (function () {
	    function GatesResultLayoutCusto() {
	    }
	    GatesResultLayoutCusto.handleActiveTab = function (searchInterface, tabName) {
	        if (tabName == 'People') {
	            Coveo.state(searchInterface.el, 'layout', 'card');
	            // GatesResultLayoutCusto.disableLayout('list');
	            GatesResultLayoutCusto.enableRecommendedResultList();
	        }
	        else {
	            Coveo.state(searchInterface.el, 'layout', 'list');
	            // GatesResultLayoutCusto.disableLayout('card');
	            GatesResultLayoutCusto.disableRecommendedResultList();
	        }
	    };
	    GatesResultLayoutCusto.disableLayout = function (layout) {
	        var resultLayouts = document.getElementsByClassName('CoveoResultLayout');
	        _.each(resultLayouts, function (resultLayout) {
	            var resultLayoutDom = Coveo.$$(resultLayout);
	            var resultLayoutComponent = Coveo.Component.get(resultLayoutDom.el);
	            resultLayoutComponent.disableLayouts([layout]);
	        });
	    };
	    GatesResultLayoutCusto.enableRecommendedResultList = function () {
	        var recommendedResultLists = document.getElementsByClassName('CoveoRecommendedResultList');
	        _.each(recommendedResultLists, function (recommendedResultList) {
	            var recommendedResultListDom = Coveo.$$(recommendedResultList);
	            var recommendedResultListComponent = Coveo.Component.get(recommendedResultListDom.el);
	            recommendedResultListComponent.enable();
	        });
	    };
	    GatesResultLayoutCusto.disableRecommendedResultList = function () {
	        var recommendedResultLists = document.getElementsByClassName('CoveoRecommendedResultList');
	        _.each(recommendedResultLists, function (recommendedResultList) {
	            var recommendedResultListDom = Coveo.$$(recommendedResultList);
	            var recommendedResultListComponent = Coveo.Component.get(recommendedResultListDom.el);
	            recommendedResultListComponent.disable();
	        });
	    };
	    return GatesResultLayoutCusto;
	}());
	exports.GatesResultLayoutCusto = GatesResultLayoutCusto;


/***/ }),
/* 12 */
/***/ (function(module, exports) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var Initialization = Coveo.Initialization;
	var ComponentOptions = Coveo.ComponentOptions;
	/**
	 * Component used to render Recommended Results either based on ML tuning relevance or top result feature
	 */
	var RecommendedResultList = (function (_super) {
	    __extends(RecommendedResultList, _super);
	    function RecommendedResultList(element, options, bindings, elementClassId) {
	        var _this = this;
	        if (elementClassId === void 0) { elementClassId = RecommendedResultList.ID; }
	        _super.call(this, element, options, bindings, elementClassId);
	        this.element = element;
	        this.options = options;
	        this.bindings = bindings;
	        this.options = ComponentOptions.initComponentOptions(element, RecommendedResultList, options);
	        if (this.enable) {
	            this.bind.onRootElement(Coveo.QueryEvents.preprocessResults, function (args) { return _this.handlePreprocessResults(args); });
	            this.bind.onRootElement(Coveo.QueryEvents.preprocessMoreResults, function (args) { return _this.handlePreprocessResults(args); });
	            this.bind.onRootElement(Coveo.QueryEvents.queryError, function () { return _this.handleNoRecommendations(); });
	            this.bind.onRootElement(Coveo.QueryEvents.noResults, function () { return _this.handleNoRecommendations(); });
	            // forcing not use infinite scroll on this component.
	            this.options.enableInfiniteScroll = false;
	            Coveo.$$(this.element).prepend(Coveo.$$('div', {
	                className: 'coveo-recommended-result-list-header'
	            }, Coveo.l('RecommendedResultListTitle')).el);
	        }
	    }
	    RecommendedResultList.prototype.handlePreprocessResults = function (data) {
	        Coveo.Assert.exists(data);
	        Coveo.Assert.exists(data.results);
	        var recommendedResults = _.filter(data.results.results, function (result) {
	            return (result.isRecommendation || result['isTopResult']);
	        });
	        if (this.options.numberOfRecommendation > 0) {
	            recommendedResults = _.first(recommendedResults, this.options.numberOfRecommendation);
	        }
	        var otherResults = _.filter(data.results.results, function (result) {
	            return !_.some(recommendedResults, function (recommendationResult) {
	                return recommendationResult.uniqueId === result.uniqueId;
	            });
	        });
	        data.results = _.extend(data.results, {
	            recommendedResults: recommendedResults,
	            results: otherResults
	        });
	        Coveo.$$(this.root).toggleClass('coveo-no-recommendations', recommendedResults.length == 0);
	    };
	    RecommendedResultList.prototype.handleNoRecommendations = function () {
	        Coveo.$$(this.root).addClass('coveo-no-recommendations');
	    };
	    /**
	     * Overriding buildResults from Parent (ResultList) to make sure the list
	     * is iterating against the recommended Results populating during preprocess results event.
	     * Builds and returns an array of HTMLElement with the given result set.
	     * @param results the result set to build an array of HTMLElement from.
	     */
	    RecommendedResultList.prototype.buildResults = function (results) {
	        var _this = this;
	        var res = [];
	        _.each(results['recommendedResults'], function (result) {
	            var resultElement = _this.buildResult(result);
	            if (resultElement != null) {
	                res.push(resultElement);
	            }
	        });
	        Coveo.ResultList.resultCurrentlyBeingRendered = null;
	        return res;
	    };
	    RecommendedResultList.prototype.disable = function () {
	        this.options.enable = false;
	    };
	    RecommendedResultList.prototype.enable = function () {
	        this.options.enable = true;
	    };
	    RecommendedResultList.ID = 'RecommendedResultList';
	    /**
	     * The options for the component
	     * @componentOptions
	     */
	    RecommendedResultList.options = {
	        /**
	         * Specifies a maximum number of recommendations, if set to 0 all recommended results will be
	         * moved the recommendedResultList component.
	         */
	        numberOfRecommendation: ComponentOptions.buildNumberOption({ defaultValue: 3 }),
	        enable: ComponentOptions.buildBooleanOption({ defaultValue: true })
	    };
	    return RecommendedResultList;
	}(Coveo.ResultList));
	exports.RecommendedResultList = RecommendedResultList;
	Initialization.registerAutoCreateComponent(RecommendedResultList);


/***/ })
/******/ ])
});
;
//# sourceMappingURL=Coveo.Gates.js.map